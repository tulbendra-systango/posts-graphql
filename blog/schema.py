import graphene

import posts.schema as post_schema


class Query(post_schema.Query, graphene.ObjectType):
    pass


class Mutation(post_schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
