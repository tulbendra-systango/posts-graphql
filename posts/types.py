import django_filters
from graphene import relay
from django.contrib.auth import get_user_model
from graphene_django.types import DjangoObjectType

from posts.models import Post, Comment, Advertisement


class UserType(DjangoObjectType):

    class Meta:
        model = get_user_model()
        exclude = ('password',)


class PostType(DjangoObjectType):

    class Meta:
        model = Post
        fields = '__all__'
        interfaces = (relay.Node,)


class PostFilter(django_filters.FilterSet):
    author = django_filters.CharFilter(field_name='author__first_name', lookup_expr='icontains')

    class Meta:
        model = Post
        fields = {
            "title": ["icontains", "iexact", "istartswith"]
        }


class CommentType(DjangoObjectType):

    class Meta:
        model = Comment
        fields = '__all__'


class AdvertisementType(DjangoObjectType):

    class Meta:
        model = Advertisement
        fields = '__all__'
