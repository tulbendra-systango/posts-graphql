from rest_framework import serializers

from .models import Post, Comment, Advertisement


class PostSerializer(serializers.Serializer):

    class Meta:
        model = Post
        fields = '__all__'


class CommentSerializer(serializers.Serializer):

    class Meta:
        model = Comment
        fields = '__all__'


class AdvertisemntSerializer(serializers.Serializer):

    class Meta:
        model = Advertisement
        fields = ('img_url', 'advertise_text', 'post')
