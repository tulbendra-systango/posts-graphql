import graphene
import graphql_jwt
from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required
from graphene_django.filter import DjangoFilterConnectionField


from posts.models import Post, Comment
from posts.mutations import (
    UpdatePost, CreateComment, CreatePost, UpdateComment, CreateUser, AdvertisementMutation, DeletePost,
    DeleteUser, DeleteComment
)
from posts.types import PostType, CommentType, UserType, PostFilter


class Query(graphene.ObjectType):
    posts = DjangoFilterConnectionField(PostType, filterset_class=PostFilter)
    post = graphene.relay.Node.Field(PostType, post_id=graphene.Int())
    comments = graphene.List(CommentType)
    users = graphene.List(UserType)
    user = graphene.Field(UserType, user_id=graphene.Int())
    me = graphene.Field(UserType)

    @login_required
    def resolve_posts(self, info, **kwargs):
        return Post.objects.all()

    def resolve_post(self, info, post_id):
        return Post.objects.get(id=post_id)

    def resolve_comments(self, info, **kwargs):
        return Comment.objects.all()

    def resolve_users(self, info, **kwargs):
        return get_user_model().objects.all()

    def resolve_user(self, info, user_id):
        return get_user_model().objects.get(id=user_id)

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception("User Not logged in")
        return user


class Mutation(graphene.ObjectType):
    update_post = UpdatePost.Field()
    create_post = CreatePost.Field()
    create_comment = CreateComment.Field()
    update_comment = UpdateComment.Field()
    create_user = CreateUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    advertisement = AdvertisementMutation.Field()
    delete_user = DeleteUser.Field()
    delete_post = DeletePost.Field()
    delete_comment = DeleteComment.Field()



