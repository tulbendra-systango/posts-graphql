from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')

    def __str__(self):
        return self.title


class Comment(models.Model):
    comment_text = models.TextField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return self.comment_text


class Advertisement(models.Model):
    img_url = models.CharField(max_length=100)
    advertise_text = models.CharField(max_length=100, null=True, blank=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="advertisements")

    def __str__(self):
        return self.img_url