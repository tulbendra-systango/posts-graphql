import graphene
from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required
from graphene_django.rest_framework.mutation import SerializerMutation

from .models import Post, Comment
from .serializers import AdvertisemntSerializer
from .types import PostType, CommentType, UserType, AdvertisementType


class UpdatePost(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        title = graphene.String()
        content = graphene.String()
        author = graphene.Int()

    post = graphene.Field(PostType)

    def mutate(self, info, id, title, content, author):
        post = Post.objects.get(pk=id)
        post.title = title
        post.content = content
        post.author = get_user_model().objects.get(pk=author)
        post.save()
        return UpdatePost(post=post)


class CreatePost(graphene.Mutation):
    class Arguments:
        title = graphene.String()
        content = graphene.String()
        author = graphene.Int()

    post = graphene.Field(PostType)

    def mutate(self, info, title, content, author):
        post = Post()
        post.title = title
        post.content = content
        post.author = get_user_model().objects.get(pk=author)
        post.save()
        return CreatePost(post=post)


class DeletePost(graphene.Mutation):
    class Arguments:
        id = graphene.Int()

    post_deleted = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        post = Post.objects.get(pk=id)
        post.delete()
        return DeletePost(post_deleted=True)


class CreateComment(graphene.Mutation):
    class Arguments:
        comment_text = graphene.String()
        post = graphene.Int()

    comment = graphene.Field(CommentType)

    def mutate(self, info, comment_text, post):
        comment = Comment()
        comment.comment_text = comment_text
        comment.post = Post.objects.get(pk=post)
        comment.save()
        return CreateComment(comment=comment)


class UpdateComment(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        comment_text = graphene.String()
        post = graphene.Int()

    comment = graphene.Field(CommentType)

    def mutate(self, info, id, comment_text, post):
        comment = Comment.objects.get(pk=id)
        comment.comment_text = comment_text
        comment.post = Post.objects.get(pk=post)
        comment.save()
        return UpdateComment(comment=comment)


class DeleteComment(graphene.Mutation):
    class Arguments:
        id = graphene.Int()

    comment_deleted = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        comment = Comment.objects.get(pk=id)
        comment.delete()
        return DeleteComment(comment_deleted=True)


class CreateUser(graphene.Mutation):

    class Arguments:
        user_name = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()
        email = graphene.String()
        password = graphene.String()

    user = graphene.Field(UserType)

    def mutate(self, info, user_name, first_name, last_name, email, password):
        user = get_user_model()(email=email, username=user_name, first_name=first_name, last_name=last_name)
        user.set_password(password)
        user.save()
        return CreateUser(user=user)


class DeleteUser(graphene.Mutation):
    class Arguments:
        id = graphene.Int()

    user_deleted = graphene.Boolean()

    def mutate(self, info, id):
        user = get_user_model().objects.get(pk=id)
        user.delete()
        return DeleteUser(user_deleted=True)


class AdvertisementMutation(SerializerMutation):
    class Meta:
        serializer_class = AdvertisemntSerializer
        model_operation = ['crated', 'update']
        lookup_field = 'id'
